def list_if_found(my_list, num,word):
    if num in my_list:
        return f'I found {num}'
    elif word in my_list:
        return f'I found {word}'
    else: print('Sorry,I cant find anything...')

def main():
    lst_str=input('Введите список через точку с запятой:')
    lst=lst_str.split(';')
    num=input('Введите число:')
    word=input('Введите строку:')
    print(list_if_found(lst_str,num,word))


if __name__=='__main__':
    main()