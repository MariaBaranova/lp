def set_print_info_for_2(my_set_left, my_set_right):
    if my_set_left == my_set_right :
        return 'Множества равны'
    elif my_set_left.issubset(my_set_right):
        return 'Левое множество является подмножеством правого'
    elif my_set_right.issubset(my_set_left):
        return 'Правое множество является подмножеством левого'
    else: return 'Общие эелементы множества:' + str(my_set_left.intersection(my_set_right))



def main():
    set_left = set(input('Введитеэлементы левого множества'))
    set_right = set(input('Введите эелементы правого множества'))
    print(set_print_info_for_2(set_left,set_right))


if __name__=='__main__':
    main()