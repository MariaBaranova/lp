def list_count_word_num(my_list):
    lst = my_list.split(';')
    lst_num = []
    lst_word = []
    for p in lst:
        if p.isdigit():
            lst_num.append(p)
        else:
            lst_word.append(p)
    print(f'Количество чисел:{len(lst_num)}')
    print(f'Количество слов:{len(lst_word)}')
def main():
    lst_str = input('Введите список через точку с запятой:')
    print(list_count_word_num(lst_str))
if __name__ == '__main__':
    main()