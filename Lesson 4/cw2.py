from itertools import zip_longest


def zip_name_mark(names, marks):
    return zip_longest(names,marks, fillvalue='!!!')



def main():
   names = ['a','b','c','d']
   marks = [3, 5,]
   print(list(zip_name_mark(names,marks)))

if __name__=='__main__':
    main()