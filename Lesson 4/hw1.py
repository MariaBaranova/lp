def gen_list_double(n):
    return [n*2
            for n
            in range(0,n)]
def main():
    print(gen_list_double(3))


if __name__ == '__main__':
        main()