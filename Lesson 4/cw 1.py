def convert_set_to_dict(my_set, val):
    my_dict={}
    for i, k in enumerate(my_set):
        my_dict[k] = i **2

    val_in_keys = True if val in my_dict.keys() else False
    val_in_vals = True if val in my_dict.values() else False
    return my_dict, val_in_keys, val_in_vals



def main():
   my_set={2, 3, True, 999, 'my'}
   print(convert_set_to_dict(my_set, 3))
   print(convert_set_to_dict(my_set, False))

if __name__=='__main__':
    main()