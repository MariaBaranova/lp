from itertools import zip_longest


def zip_names(fnames, snames):
    return zip_longest(fnames,snames, fillvalue='неизвестно')



def main():
   fnames = ['Маша','Юля','Артем','Света']
   snames = ['Баранова', 'Наговицына']
   print(list(zip_names(fnames,snames)))

if __name__=='__main__':
    main()