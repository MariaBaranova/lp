def gen_list_pow_start_stop(start,stop):
    return [n**2
            for n
            in range(start,stop)]
def main():
    print(gen_list_pow_start_stop(3, 6))


if __name__ == '__main__':
        main()