def gen_dict_double(n):
    dict_double = {n:n*2 for n in range(0, n)}
    return dict_double
def main():
    print(gen_dict_double(3))


if __name__ == '__main__':
        main()