def gen_list_pow_odd(n):
    return [n**2
            for n
            in range(0,n) if n%2 != 0]
def main():
    print(gen_list_pow_odd(7))


if __name__ == '__main__':
        main()