class Goat:
    def __init__(self, age, name, place_number, milk_volume):
        self.age = age
        self.name = name
        self.place_number = place_number
        self.milk_volume = milk_volume
    def goat_sound(self):
        print 'мемеме'
    def goat_param(self):
        prin(f'имя козы-{self.name}, возраст козы- {self.age}')
    def goat_place_number(self):
        return self.place_number
    def goat_milk_volume(self):
        return self.milk_volume
class Chiken:
    def __init__(self, name, place, egg_number):
        self.name = name
        self.place = place
        self.egg_number = egg_number
    def chiken_sound(self):
        print 'кококо'
    def chiken_name(self):
        prin(f'имя курицы-{self.name}')
    def chiken_place(self):
        return self.place
    def chiken_egg_number(self):
        return self.egg_number
class Ferm(Goat, Chiken):
    def __init__(self, animals, ferm_name, dir_name,):
        self.animals = []
        self.ferm_name = ferm_name
        self.dir_name = dir_name
    def farm_dir_name(self):
        print(f'Наименование фермы - {self.ferm_name}, имя владельца - {self.dir_name}')
    def extend(self, *animals):
        self.animals.extend(animals)
    def goat_count(self):
        return len ([animal for animals in self.animals if type(animal) == Goat])
    def chiken_count(self):
        return len ([animal for animals in self.animals if type(animal) == Chiken])