class Dog:
    def __init__(self, age, name):
        self.age = age
        self.name = name

    def print_park(self):
        print(f'Гав Гав Гав - это мое имя {self.name} на собачьем языке')

    def get_age(self):
        return self.age

my_dog = Dog(12, 'Chip')
print (my_dog.print_park())
print (my_dog.get_age())