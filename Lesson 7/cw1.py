def test(func):
    import time

    def wrapper(*args):
        print("Я получил: ", args)
        start = time.time_ns()
        func(*args)
        end = time.time_ns()