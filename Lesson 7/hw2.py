class Zoo:
    def __init__(self,animal_count, name, money):
        self.animal_count = animal_count
        self.name= name
        self.money= money
    def get_animal_count(self):
        return self.animal_count
    def print_name(self):
        print (f'{self.name}- это лучший зоопарк')
    def can_afford(self, needed_money):
        if self.money >= needed_money: return True
        else: False
my_zoo= Zoo(50, 'Izh', 1000000)
print( my_zoo.get_animal_count())
my_zoo.print_name()
print(my_zoo.can_afford(5000))
