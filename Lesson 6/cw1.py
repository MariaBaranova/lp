# Класс Страница
# Атрибуты: номер страницы, контент
# Методы: вывести контент, поиск строки в контенте

# Класс Книга
# Атрибуты: страницы, название, автор, год
# Методы: вывести весь конткнт, добавить страницу в конец,
# получить количество страниц, вернуть нечетные старницы

class Paper:
    def __init__(self, num, cnt):
        self.number = num
        self.content = cnt

    def print_content(self):
        print(self.content)

    def find_str_in_content(self, string):
        return self.content.find(string)


class Book:
    def __init__(self, title, author, year):
        self.pages = []
        self.title =title
        self.author = author
        self.year = year

    def print_content(self):
        for page in self.pages:
            page.print_content()
    def append(self, page):
        self.pages.append(page)

    def append(self, *pages):
        self.pages.extend(pages)

    def get_page_count(self):
        return len(self.pages)

    def get_even_pages(self):
        return [p for p in self.pages if p.number%2 == 0]

def main():
    #создаем и заолняем книгу страницами
    book = Book(title='Kolobook', author='Narod', year=2020)
    page_1 = Paper(num=1, cnt='Intraduction')
    book.append(page_1)
    book.append(Paper(num=2, cnt='Method'))
    book.append(Paper(num=3, cnt='Instruments')), Paper(num=4, cnt='Conclusion')
    print(book.title)
    book.print_content()
    print(book.get_page_count())
    print(book.get_even_pages())

if __name__ == '__main__':
    main()

