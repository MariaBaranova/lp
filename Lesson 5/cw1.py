def hello_with_default_vals(name="Иван", lastname="Иванов", age=18):
    if not (type(name) == str and type(lastname) == str and (type(age) == int and 0<age<150)):
        return None
    return f'Здравствуйте {name} {lastname}! {age} лет - самый лучший возраст!'



def main():
    print(hello_with_default_vals())
    print(hello_with_default_vals('Petr'))
    print(hello_with_default_vals(lastname='Petrov'))
    print(hello_with_default_vals(age=99))
    print(hello_with_default_vals('Boris','Borisov'))
    print(hello_with_default_vals('Misha', age=38))
    print(hello_with_default_vals(lastname='Godunov', age=67))
    print(hello_with_default_vals('Kate','Katerinova', age=20))

if __name__=='__main__':
    main()