def print_fname_sname_flower_height(fname, sname, **height):
    print(f'Owner: {fname} {sname} Flowers: {height}')


def main():
    print_fname_sname_flower_height('Лидия', 'Петрова', rose=[50, 77, 80], tulp=60, magic_flower=[444,555,777])


if __name__=='__main__':
    main()