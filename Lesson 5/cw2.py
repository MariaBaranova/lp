def print_flower_price(flower_name, *prices):
    prices_str = ' '.join(str(elem) for elem in prices)
    print("{0}:{1}".format(flower_name, prices_str))

    print(flower_name, *prices)


def main():
    print_flower_price('rose', 77, 10, 50, 99)
    print_flower_price('tulp', 100, 44, 777, 876, 555, 111)

if __name__=='__main__':
    main()